/**
* Copyright 2017 yarl.
*
* This file is part of jackfs.
*
* jackfs is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* jackfs is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with jackfs.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <error.h>
#include <string.h>
#include <stdlib.h>
#include "utils.h"

void *checkalloc(void *ptr)
{
	if (!ptr)
		error(1, 0, "OOM");
	return ptr;
}

char *copy_string(char const *s)
{
	char *ret = checkalloc(strdup(s));
	return ret;
}

/*symbol
str2sym(char const* str)
{
	ENTRY *e = hsearch((ENTRY){str,str}, ENTER);
	if(!e)
		error(1, errno, "htable problem");
	return (symbol)e->data;
}*/

int int_min(int a, int b)
{
	if (a <= b)
		return a;
	return b;
}

int equal_str(char const *s1, char const *s2)
{
	if (!s1 && !s2)
		return 1;
	if ((s1 && !s2) || (!s1 && s2))
		return 0;
	return !strcmp(s1, s2);
}

void freenull(void **pptr)
{
	free(*pptr);
	*pptr = NULL;
}

void jackfs_error(int errno, char const *msg)
{
	error(0, errno, msg);
}
