/**
* Copyright 2017 yarl.
*
* This file is part of jackfs.
*
* jackfs is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* jackfs is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with jackfs.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _UTILS_H
#define _UTILS_H

void* checkalloc(void*);
char* copy_string(char const*);
typedef char const* symbol;
int int_min(int,int);
void freenull(void**);
int equal_str(char const* s1, char const* s2);
void jackfs_error(int,char const*);

#endif // _UTILS_H
