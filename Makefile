CC	?= gcc
LIBS ?= fuse
CFLAGS ?= -L /usr/local/lib -I /usr/local/include -O0 -g -Wall `pkg-config $(LIBS) --cflags --libs` -ljack -lpthread

PROG = jackfs
OBJS = fs.o node.o jack.o utils.o notification.o shared.o

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) -g $(CFLAGS) -o $(PROG) $(OBJS)

.c.o:
	$(CC) $(CFLAGS) -c $*.c

clean:
	rm -rf $(PROG) $(OBJS)
