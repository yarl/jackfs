/**
* Copyright 2017 yarl.
*
* This file is part of jackfs.
*
* jackfs is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* jackfs is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with jackfs.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string.h>
#include <stdlib.h>
#include <limits.h>		//realpath
#include <error.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <libgen.h>

#include <assert.h>
#include <stdio.h>

#include <jack/jack.h>
#include <pthread.h>

#include "node.h"
#include "utils.h"
#include "jack.h"
#include "shared.h"

Node *root;

static void addChild(Node * p, Node * n)
{
	assert(p->type == DIRNODE);
	SLIST_INSERT_HEAD(&AS(DirNode *, p)->children, n, siblings);
}

static Node *node_alloc(NodeType type)
{
	size_t size = 0;
	switch (type) {
	case DIRNODE:
		size = sizeof(DirNode);
		break;
	case SYMLINKNODE:
		size = sizeof(SymlinkNode);
		break;
	}
	return checkalloc(calloc(1, size));
}

static Node *newNode(char const *name, Node * parent, NodeType type)
{
	assert(!parent || parent->type == DIRNODE);
	Node *ret = node_alloc(type);
	ret->type = type;
	ret->name = copy_string(name);

	if (parent) {
		ret->parent = parent;
		++parent->nlink;
		addChild(parent, ret);
	} else {		//my parent, myself and I
		assert(type == DIRNODE);
		ret->parent = ret;
	}
	if (type == DIRNODE) {
		ret->nlink = 2;
		AS(DirNode *, ret)->pid = -1;
		SLIST_INIT(&((DirNode *) ret)->children);
	} else {
		ret->nlink = 1;
	}
	return ret;
}

Node *newDirNode(char const *name, Node * parent)
{
	return newNode(name, parent, DIRNODE);
}

static void execNode(Node * n)
{
	pthread_mutex_lock(&wait_pid_mutex);
	int pid = fork();
	if (-1 == pid)
		error(1, 0, "fork error");
	else if (pid != 0) {
		AS(DirNode *, n->parent)->pid = pid;

		wait_pid = pid;
		wait_pid_name = NULL;
		while (!wait_pid_name)
			pthread_cond_wait(&wait_pid_cond, &wait_pid_mutex);	//TODO: Use timedwait
		AS(DirNode *, n->parent)->client_name = wait_pid_name;
		pthread_mutex_unlock(&wait_pid_mutex);
		return;
	}			// else in child
	SymlinkNode *sln = AS(SymlinkNode *, n);
	char *str = copy_string(sln->target);
	char *dir = dirname(str);
	if (chdir(dir))
		error(1, errno, "chdir");
	free(str);
	char *argv[] = { sln->target, NULL };
	if (execv(sln->target, argv))
		error(1, errno, "execve error");
}

Node *newSymlinkNode(char const *name, Node * parent, char const *target)
{
	Node *ret = newNode(name, parent, SYMLINKNODE);
	AS(SymlinkNode *, ret)->target = copy_string(target);
	execNode(ret);
	return ret;
}

void removeChild(Node * p, Node * c)
{
	assert(p->type == DIRNODE);
	SLIST_REMOVE(&AS(DirNode *, p)->children, c, Node, siblings);
}

static int isRunning(Node * n)
{
	assert(n->type == DIRNODE);
//  return AS(DirNode*,n)->target != NULL;
	return 1;
}

int isRoot(Node * n)
{
	if (n->type != DIRNODE)
		return 0;
	if (n->parent == n)
		return 1;
	return 0;
}

void deleteNode(Node * this)
{
	if (isRoot(this))
		root = NULL;
	else
		removeChild(this->parent, this);
	--this->parent->nlink;
	DirNode *dthis = NULL;
	switch (this->type) {
	case DIRNODE:

		break;
	case SYMLINKNODE:	// it is 'exec'
		assert((0 == strcmp(this->name, "exec")));
		free(AS(SymlinkNode *, this)->target);
		dthis = AS(DirNode *, this->parent);
		freenull((void **)&dthis->client_name);
		if (kill(dthis->pid, SIGTERM))
			jackfs_error(errno, "kill fail");
		waitpid(dthis->pid, NULL, 0);	//TODO: integrate into a general SIGCHLD handling
		freenull((void **)&dthis->input_ports.a);
		dthis->input_ports.n = 0;
		freenull((void **)&dthis->output_ports.a);
		dthis->output_ports.n = 0;
		break;
	}
	free(this->name);
	free(this);
}

static void deleteNodeR(Node * this)
{
	if (this->type == DIRNODE) {
		DirNode *dthis = AS(DirNode *, this);
		for (Node * n = SLIST_FIRST(&dthis->children); n;
		     n = SLIST_FIRST(&dthis->children)) {
			deleteNodeR(n);
			SLIST_REMOVE_HEAD(&dthis->children, siblings);
		}
	}
	deleteNode(this);
}

Node *child(Node const *parent, char const *str)
{
	if (parent->type != DIRNODE)
		return NULL;
	Node *n;
	SLIST_FOREACH(n, &AS(DirNode const *, parent)->children, siblings) {
		if (strcmp(n->name, str) == 0)
			return n;
	}
	return NULL;
}

void nodes_cleanup(void)
{
	deleteNodeR(root);
}

Node *newRoot()
{
	if (root)
		error(1, 0, "There's already a root");
	Node *ret = newNode("/", NULL, DIRNODE);
	root = ret;

	return ret;
}

Node *lookup(char const *_path)
{
	if (!root)
		return NULL;
	if (0 == strcmp(_path, "/"))
		return root;
	char *path = copy_string(_path);
	Node *n = root, *c = root;
	char *tok;
	for (tok = strtok(path, "/"); tok; tok = strtok(NULL, "/"), n = c) {
		c = child(n, tok);
		if (!c)
			break;
	}
	free(path);
	return c;
}

static Node *lbcn(Node * n, char const *name)
{
	if (n->type != DIRNODE)
		return NULL;
	DirNode *dn = AS(DirNode *, n);
	if (equal_str(dn->client_name, name))
		return n;
	Node *c;
	SLIST_FOREACH(c, &dn->children, siblings) {
		if (c->type == DIRNODE) {
			if (equal_str(AS(DirNode *, c)->client_name, name))
				return c;
			return lbcn(c, name);
		}
	}
	return NULL;
}

Node *lookup_by_client_name(char const *name)
{
	if (!root)
		return NULL;
	return lbcn(root, name);
}

static void add_port_aux(PortArray * pa, jack_port_t * port)
{
	++pa->n;
	checkalloc(pa->a = realloc(pa->a, pa->n));
	pa->a[pa->n - 1] = port;
}

static void remove_port_aux(PortArray * pa, jack_port_t * port)
{				//no free??
	int n = pa->n;
	int i;
	for (i = 0; i < n; ++i) {
		if (pa->a[i] == port)
			break;
	}
	assert(i < n);
	for (; i < n - 1; ++i)
		pa->a[i] = pa->a[i + 1];
	--pa->n;
}

/**
 *	Connect a node's i'th output port to it's parent's i'th input port
 *	Only if parents has i or more input ports, and I
 */
extern int PORT_NAME_SIZE;
char *sysportname[2] = { "system:playback_1", "system:playback_2" };

extern jack_client_t *jack_client;
static void node_jack_connect(Node * n, int i)
{
	DirNode *dn = AS(DirNode *, n);
	DirNode *pdn = AS(DirNode *, n->parent);
	if (isRoot(n)) {
		if (i < 2)
			jack_connect(jack_client,
				     jack_port_name(dn->output_ports.a[i]),
				     sysportname[i]);
	} else {
		if (pdn->input_ports.n > i)
			my_jack_connect(dn->output_ports.a[i],
					pdn->input_ports.a[i]);
	}
}

/**
 * Do that for every children of n
 */
static void connect_children(Node * n, int i)
{
	DirNode *dn = AS(DirNode *, n);
	Node *c;
	SLIST_FOREACH(c, &AS(DirNode *, n)->children, siblings) {
		if (c->type == DIRNODE)
			node_jack_connect(c, i);
	}
}

void addInPort(Node * n, jack_port_t * port)
{
	assert(n->type == DIRNODE);
	DirNode *dn = AS(DirNode *, n);
	add_port_aux(&dn->input_ports, port);
	connect_children(n, dn->input_ports.n - 1);
}

void addOutPort(Node * n, jack_port_t * port)
{
	assert(n->type == DIRNODE);
	DirNode *dn = AS(DirNode *, n);
	add_port_aux(&dn->output_ports, port);
	node_jack_connect(n, dn->output_ports.n - 1);
}

void removeInPort(Node * n, jack_port_t * port)
{
	assert(n->type == DIRNODE);
	remove_port_aux(&AS(DirNode *, n)->input_ports, port);
}

void removeOutPort(Node * n, jack_port_t * port)
{
	assert(n->type == DIRNODE);
	remove_port_aux(&AS(DirNode *, n)->output_ports, port);
}

void nodes_lock(void)
{
	pthread_mutex_lock(&nodes_mutex);
}

void nodes_release(void)
{
	pthread_mutex_unlock(&nodes_mutex);
}

static int isPort(Node * n, jack_port_t * port, int flags)
{
	assert(n->type == DIRNODE);
	DirNode *dn = AS(DirNode *, n);
	PortArray *pa =
	    (JackPortIsInput & flags) ? &dn->input_ports : &dn->output_ports;
	for (int i = 0; i < pa->n; ++i) {
		if (pa->a[i] == port)
			return i;
	}
	return 0;
}

void portRegistration(Node * n, jack_port_t * port, int reg)
{
	int flags = jack_port_flags(port);
	int is_port = isPort(n, port, flags);
	if (reg && !is_port)
		(flags & JackPortIsInput) ? addInPort(n, port) : addOutPort(n,
									    port);
	else if (!reg && is_port)
		(flags & JackPortIsInput) ? removeInPort(n,
							 port) :
		    removeOutPort(n, port);
}

static void client_reg(char *name, int reg)
{
	if (reg) {
		int pid = jack_client_get_pid(jack_client, name);
		if (!pid) {
			return;	//we don't care. That probably never occurs
		}
		pthread_mutex_lock(&wait_pid_mutex);
		//wait_pid_mutex protects wait_pid and wait_pid_name, no need to nodes_lock()
		if (wait_pid == pid) {
			wait_pid_name = copy_string(name);
			wait_pid = -1;
			pthread_cond_signal(&wait_pid_cond);
		}		// else we we're not waiting for that pid
		pthread_mutex_unlock(&wait_pid_mutex);
	}
  /** else unregistration
  * if that's a client we aren't controlling, we don't care.
  * if we are, then either we killed it and jack is just confirming it, either we didn't
  * and it was killed for a ... hmm ... bad reason. Anyway, we use SIGCHLD to handle these
  */
}

static void port_reg(char *name, int reg)
{
	jack_port_t *port = jack_port_by_name(jack_client, name);	//a check please
	char *client_name = strtok(name, ":");
	nodes_lock();
	Node *n = lookup_by_client_name(client_name);
	if (n)
		portRegistration(n, port, reg);
	nodes_release();
}

void notification_consumer(Notification * not)
{
	char *name = not->name;
	int reg = not->reg;
	switch (not->type) {
	case CLIENT_REG:
		client_reg(name, reg);
		break;
	case PORT_REG:
		port_reg(name, reg);
		break;
	default:
		printf("Unkown notification received\n");
	}
	freeNotification(not);
}
