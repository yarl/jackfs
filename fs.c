/**
* Copyright 2017 yarl.
*
* This file is part of jackfs.
*
* jackfs is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* jackfs is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with jackfs.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
  gcc -Wall fs.c `pkg-config fuse --cflags --libs` -o fs
*/

#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <error.h>
#include <errno.h>
#include <fcntl.h>		// S_IFDIR, ...
#include <stdlib.h>		//alloc
#include <libgen.h>		//basename, dirname
#include <fnmatch.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include <assert.h>

#include "node.h"
#include "utils.h"
#include "jack.h"
#include "shared.h"

static int _getattr(const char *path, struct stat *stbuf)
{
	memset(stbuf, 0, sizeof(struct stat));

	nodes_lock();
	Node *n = lookup(path);
	nodes_release();

	if (!n)
		return -ENOENT;

	stbuf->st_nlink = n->nlink;

	switch (n->type) {
	case DIRNODE:
		stbuf->st_mode = S_IFDIR | 0755;
		break;
/*		case REGNODE:
			stbuf->st_mode = S_IFREG | 0444;
			stbuf->st_size = AS(RegNode*,n)->size;
			break;*/
	case SYMLINKNODE:
		stbuf->st_mode = S_IFLNK | 0777;
		stbuf->st_size =
		    AS(SymlinkNode *,
		       n)->target ? strlen(AS(SymlinkNode *, n)->target) : 0;
		break;
	}
	return 0;
}

static int _mkdir(const char *_path, mode_t mode)
{
	(void)mode;
	char *path = copy_string(_path);	// dirname/basename can modify input
	char *dir = dirname(path);

	nodes_lock();
	Node *n = lookup(dir);
	free(path);

	if (!n) {
		nodes_release();
		return -ENOENT;
	}

	if (n->type != DIRNODE) {
		nodes_release();
		return -ENOTDIR;
	}

	path = copy_string(_path);	// we need a fresh path
	char *name = basename(path);
	if (child(n, name)) {
		nodes_release();
		free(path);
		return -EEXIST;
	}
	//we can make a new node here
	newDirNode(name, n);
	nodes_release();

	free(path);
	return 0;
}

static int _unlink(const char *path)
{
	nodes_lock();
	Node *n = lookup(path);

	if (!n) {
		nodes_release();
		return -ENOENT;
	}

	if (n->type == DIRNODE) {
		nodes_release();
		return -EISDIR;
	}

	deleteNode(n);
	nodes_release();
	return 0;
}

static int
_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
	 off_t offset, struct fuse_file_info *fi)
{
	(void)offset;
	(void)fi;

	nodes_lock();
	Node *n = lookup(path);

	if (!n) {
		nodes_release();
		return -ENOENT;
	}

	if (n->type != DIRNODE) {
		nodes_release();
		return -ENOTDIR;
	}

	filler(buf, ".", NULL, 0);
	filler(buf, "..", NULL, 0);

	Node *c;
	SLIST_FOREACH(c, &AS(DirNode *, n)->children, siblings) {
		filler(buf, c->name, NULL, 0);
	}
	nodes_release();
	return 0;
}

static int _readlink(const char *path, char *buf, size_t size)
{
	nodes_lock();
	Node *n = lookup(path);
	nodes_release();

	if (!n)
		return -ENOENT;

	if (n->type != SYMLINKNODE)
		return -EINVAL;

	strncpy(buf, AS(SymlinkNode *, n)->target, size - 1);
	buf[size - 1] = 0;
	return 0;
}

static int _symlink(const char *target, const char *_path)
{
	char *path = copy_string(_path);	// dirname/basename can modify input
	char *dir = dirname(path);
	nodes_lock();
	Node *n = lookup(dir);
	free(path);

	if (!n) {
		nodes_release();
		return -ENOENT;
	}

	if (n->type != DIRNODE) {
		nodes_release();
		return -ENOTDIR;
	}

	if (isRoot(n)) {
		nodes_release();
		return -EACCES;
	}

	path = copy_string(_path);	// we need a fresh path
	char *name = basename(path);
	if (strcmp(name, "exec")) {
		nodes_release();
		free(path);
		return -EACCES;
	}

	if (child(n, name)) {
		nodes_release();
		free(path);
		return -EEXIST;
	}

	newSymlinkNode(name, n, target);
	nodes_release();
	free(path);
	return 0;
}

static struct fuse_operations fs_oper = {
	.getattr = _getattr,
	.mkdir = _mkdir,
	.unlink = _unlink,
	.readdir = _readdir,
	.readlink = _readlink,
	.symlink = _symlink,
};

void jack_cleanup(void);

int main(int argc, char *argv[])
{
	init_shared();
	newRoot();
	jack_init(argv[0]);

	printf("init done\n");

	if (fuse_main(argc, argv, &fs_oper, NULL))
		error(1, 0, "fuse_main error");
	nodes_cleanup();
	jack_cleanup();
	return 0;
}
