/**
* Copyright 2017 yarl.
*
* This file is part of jackfs.
*
* jackfs is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* jackfs is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with jackfs.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "shared.h"

DEFINE_TAKETHIS(notification, Notification)
void notification_consumer(Notification * not);

void init_shared(void)
{
	if (pthread_mutex_init(&nodes_mutex, NULL))
		error(1, 0, "pthread_mutex_init(&nodes_mutex)");
	if (pthread_mutex_init(&wait_pid_mutex, NULL))
		error(1, 0, "pthread_mutex_init(&wait_pid_mutex)");
	if (pthread_cond_init(&wait_pid_cond, NULL))
		error(1, 0, "pthread_cond_init(&wait_pid_cond)");
	takethis_notification_init(&takethis_not, notification_consumer);
}
