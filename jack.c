/**
* Copyright 2017 yarl.
*
* This file is part of jackfs.
*
* jackfs is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* jackfs is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with jackfs.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <jack/jack.h>
#include <jack/control.h>
#include <error.h>
#include <stdlib.h>		//atexit
#include <stdio.h>
#include <pthread.h>
#include <string.h>

#include "node.h"
#include "utils.h"
#include "notification.h"
#include "shared.h"

void client_registration_callback(const char *name, int reg, void *arg)
{
	takethis_notification_snd(&takethis_not,
				  newNotification(CLIENT_REG, name, reg));
}

void port_registration_callback(jack_port_id_t port_id, int reg, void *arg)
{
	jack_port_t *port = jack_port_by_id(jack_client, port_id);
	char const *name = jack_port_name(port);
	takethis_notification_snd(&takethis_not,
				  newNotification(PORT_REG, name, reg));
}

void jack_cleanup(void)
{
	if (jack_client_close(jack_client))
		printf("jack_client_close failed\n");
}

static void jack_shutdown(void *arg)
{
	exit(EXIT_FAILURE);
}

void jack_init(char const *myname)
{
	printf("%s\n", __func__);
	if (NULL ==
	    (jack_client = jack_client_open(myname, JackNoStartServer, NULL)))
		error(1, 0, "jack_client_open failed");
	jack_on_shutdown(jack_client, jack_shutdown, NULL);
	jack_set_client_registration_callback(jack_client,
					      client_registration_callback,
					      NULL);
	jack_set_port_registration_callback(jack_client,
					    port_registration_callback, NULL);
	jack_activate(jack_client);
	PORT_NAME_SIZE = jack_port_name_size();
	CLIENT_NAME_SIZE = jack_client_name_size();
}

void my_jack_connect(jack_port_t * out, jack_port_t * in)
{
	char const *nin = jack_port_name(in), *nout = jack_port_name(out);
	jack_connect(jack_client, nout, nin);
}
