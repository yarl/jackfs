/**
* Copyright 2017 yarl.
*
* This file is part of jackfs.
*
* jackfs is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* jackfs is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with jackfs.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _NODE_H
#define _NODE_H

#include <sys/queue.h>
#include <sys/types.h>
#include <jack/jack.h>
#include <unistd.h>

#include "utils.h"

typedef enum NodeType{
	SYMLINKNODE,
	DIRNODE,
  //REGNODE
} NodeType;

struct Node;
typedef struct Node{
	NodeType type;
	struct Node *parent;
	char *name;
	nlink_t nlink;
	SLIST_ENTRY(Node) siblings;
} Node;

typedef struct PortArray{
	jack_port_t **a;
	int n;
} PortArray;

typedef struct DirNode{
	Node _;
	//jack stuff
	char *client_name;
	PortArray input_ports;
	PortArray output_ports;

	pid_t pid;
	
	//child stuff
	SLIST_HEAD(NodeList,Node) children;
} DirNode;

typedef struct SymlinkNode{
	Node _;
	char *target;
} SymlinkNode;

/* handle destruction of 'n', removing it from it's parent's children list.
 * Do not handle own children list */
void deleteNode(Node*);

/* returns the child of 'n' named 'name', NULL if there is not. */
Node* child(Node const* n, char const* name);
int isRoot(Node*);
Node* newRoot();

/* Search a node by it's path */
Node* lookup(char const*);
/* by it's client name */
Node* lookup_by_client_name(char const*);
Node* newSymlinkNode(char const* name, Node *parent, char const* target);
Node* newDirNode(char const* name, Node* parent);

void nodes_lock(void);
void nodes_release(void);

void portRegistration(Node*, jack_port_t*, int);
void nodes_cleanup(void);
#define AS(type,what) ((type)what)

typedef struct portreg_t{
	Node* n;
	jack_port_t* port;
	int reg;
} portreg_t;

#endif // _NODE_H
