/**
* Copyright 2017 yarl.
*
* This file is part of jackfs.
*
* jackfs is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* jackfs is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with jackfs.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "notification.h"
#include "utils.h"

#include <stdlib.h>

Notification *newNotification(NotificationType type, char const *name, int reg)
{
	Notification *n = checkalloc(calloc(1, sizeof(*n)));
	n->type = type;
	n->name = copy_string(name);
	n->reg = reg;
	return n;
}

void freeNotification(Notification * n)
{
	free(n->name);
	free(n);
}
