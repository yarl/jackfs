/**
* Copyright 2017 yarl.
*
* This file is part of jackfs.
*
* jackfs is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* jackfs is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with jackfs.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _SHARED_H
#define _SHARED_H

/**
 * Shared things (globals) between jack's notification thread (callbacks),
 * my notification thread (go thing, what's consume notification)
 * and fuse thread.
 *
 * There's "my notification thread" because we can't call a jack function that will
 * call back(notify). callback loop.
 */

#include <jack/jack.h>
#include "notification.h"
#include <takethis.h>

DECLARE_TAKETHIS(notification, Notification)
takethis_notification_t takethis_not;

pthread_mutex_t nodes_mutex; //protect all nodes
pthread_mutex_t wait_pid_mutex; //protect cond just below
pthread_cond_t wait_pid_cond; //waiting for jack to give client name of just launched application(pid)
char* wait_pid_name; //what we are waiting for..
int wait_pid; //yeah, that

jack_client_t *jack_client;
int PORT_NAME_SIZE;
int CLIENT_NAME_SIZE;

void init_shared(void);

#endif // _SHARED_H
